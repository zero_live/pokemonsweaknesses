export const Types = {
  steel: 'steel',
  water: 'water',
  bug: 'bug',
  dragon: 'dragon',
  electric: 'electric',
  ghost: 'ghost',
  fire: 'fire',
  fairy: 'fairy',
  ice: 'ice',
  fighting: 'fighting',
  normal: 'normal',
  plant: 'plant',
  psychic: 'psychic',
  stone: 'stone',
  sinister: 'sinister',
  earth: 'earth',
  poison: 'poison',
  flying: 'flying'
}

Types.all = Object.keys(Types).filter(type => type !== 'all')
