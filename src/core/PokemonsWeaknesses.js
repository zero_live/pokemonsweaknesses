import { Types } from './Types.js'

export class PokemonsWeaknesses {
  static CATALOG = {
    [Types.steel]: [Types.fire, Types.fighting, Types.earth],
    [Types.water]: [Types.electric, Types.plant],
    [Types.bug]: [Types.electric, Types.ice, Types.stone],
    [Types.dragon]: [Types.ice, Types.dragon, Types.fairy],
    [Types.electric]: [Types.earth],
    [Types.ghost]: [Types.ghost, Types.sinister],
    [Types.fire]: [Types.stone, Types.water, Types.earth],
    [Types.fairy]: [Types.poison, Types.steel],
    [Types.ice]: [Types.fighting, Types.fire, Types.stone],
    [Types.fighting]: [Types.flying, Types.psychic, Types.fairy],
    [Types.normal]: [Types.fighting],
    [Types.plant]: [Types.fire, Types.poison, Types.flying, Types.ice, Types.bug],
    [Types.psychic]: [Types.bug, Types.sinister, Types.ghost],
    [Types.stone]: [Types.water, Types.plant, Types.fighting, Types.earth, Types.steel],
    [Types.sinister]: [Types.fighting, Types.bug, Types.fairy],
    [Types.earth]: [Types.water, Types.plant, Types.ice],
    [Types.poison]: [Types.earth, Types.psychic],
    [Types.flying]: [Types.electric, Types.ice, Types.stone]
  }

  static retrieve(firstElement, secondElement) {
    const firstWeaknesses = this.CATALOG[firstElement]
    if (!secondElement) { return firstWeaknesses }

    const secondWeaknesses = this.CATALOG[secondElement]
    const weaknesses = [...firstWeaknesses, ...secondWeaknesses]
    return this.removeDuplicates(weaknesses)
  }

  static removeDuplicates(array) {
    return [...new Set(array)]
  }
}
