
class Texts {
  static en = {
    steel: 'steel',
    water: 'water',
    bug: 'bug',
    dragon: 'dragon',
    electric: 'electric',
    ghost: 'ghost',
    fire: 'fire',
    fairy: 'fairy',
    ice: 'ice',
    fighting: 'fighting',
    normal: 'normal',
    plant: 'plant',
    psychic: 'psychic',
    stone: 'stone',
    sinister: 'sinister',
    earth: 'earth',
    poison: 'poison',
    flying: 'flying',
    chooseTypes: 'Choose pokemon\'s types',
    weaknesses: 'weaknesses'
  }

  static translationsFor(label) {
    return Texts.en[label]
  }
}

export const t = (label) => {
  return Texts.translationsFor(label)
}
