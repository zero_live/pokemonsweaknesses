import { t } from '../../Texts.js'

export class TypeTemplate {
  static retrieve(type) {
    const id = type
    const classes = `button element is-capitalized is-${type}`
    const text = t(type)

    return `
      <button class="${classes}" id="${id}">
        ${text}
      </button>
    `
  }
}
