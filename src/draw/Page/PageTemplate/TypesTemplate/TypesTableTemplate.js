const STARTING_ROW = '<p class="has-centered-text">'
const ENDING_ROW = '</p>'

export class TypesTableTemplate {
  static empty() {
    return new TypesTableTemplate()
  }

  constructor() {
    this.elements = new Array()
  }

  startRow() {
    this.elements.push(STARTING_ROW)
  }

  add(element) {
    this.elements.push(element)
  }

  endRow() {
    this.elements.push(ENDING_ROW)
  }

  toTemplate() {
    return this.elements.join('')
  }
}
