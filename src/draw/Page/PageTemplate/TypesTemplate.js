import { TypesTableTemplate } from './TypesTemplate/TypesTableTemplate.js'
import { Counter } from '../../support/Counter.js'
import { TypeTemplate } from './TypeTemplate.js'
import { Types } from '../../../core/Types.js'

export class TypesTemplate {
  static retrieve() {
    const types = TypesTableTemplate.empty()
    let count = Counter.atZero()

    Types.all.forEach(type => {
      if (count.isZero()) { types.startRow() }

      types.add(TypeTemplate.retrieve(type))

      count.increase()
      if (count.isFour()) {
        types.endRow()
        count.reset()
      }
    })

    return types.toTemplate()
  }
}
