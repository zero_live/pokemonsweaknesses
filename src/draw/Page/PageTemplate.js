import { TypesTemplate } from './PageTemplate/TypesTemplate.js'
import { t } from '../Texts.js'


const WEAKNESSES = 'weaknesses'

export class PageTemplate {
  static ID = 'page'
  static CHOOSE_CLASS = 'choose'
  static WEAKNESSES = WEAKNESSES

  static retrieve() {
    return `
      <div class="columns is-centered">
        <div class="column has-text-centered is-4">
          <p class="has-centered-text title">
            ${t('chooseTypes')}
          </p>
          ${TypesTemplate.retrieve()}
          <p class="has-centered-text is-capitalized title">
            ${t('weaknesses')}
          </p>
          <p class="has-centered-text" id="${WEAKNESSES}">
          </p>
        </div>
      </div>
    `
  }
}
