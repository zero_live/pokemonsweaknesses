
export class WeaknessTemplate {
  static retrieve(type) {
    const id = `weakness-${type}`
    const classes = `button element is-capitalized is-${type}`

    return `
      <a class="${classes}" id="${id}">
        ${type}
      </a>
    `
  }
}
