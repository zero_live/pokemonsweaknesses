
export class ChosenType {
  static asNull() {
    return new ChosenType()
  }

  constructor() {
    this.value = null
  }

  as(type) {
    this.value = type
  }

  toString() {
    return this.value
  }

  is(type) {
    return this.value === type
  }

  isNull() {
    return this.value === null
  }
}
