import { PokemonsWeaknesses } from '../core/PokemonsWeaknesses.js'
import { WeaknessTemplate } from './Page/WeaknessTemplate.js'
import { PageTemplate } from './Page/PageTemplate.js'
import { ChosenType } from './Page/ChosenType.js'
import { Elements } from './support/Elements.js'
import { Types } from '../core/Types.js'

export class Page {
  static draw() {
    const elements = Elements.prepare()

    new Page(elements).draw()
  }

  constructor(elements) {
    this.elements = elements

    this.initializeChosenPokemonTypes()
  }

  draw() {
    this.html()
    this.eventListeners()
  }

  html() {
    this.elements.drawIn(PageTemplate.ID, PageTemplate.retrieve())
  }

  eventListeners() {
    Types.all.forEach(type => {
      this.elements.addOnClickTo(type, this.chooseType.bind(this, type))
    })
  }

  chooseType(type) {
    if (this.areTypesChosen()) { this.resetChosenTypes() }
    if (this.hasToSetFirstType()) { this.firstPokemonType.as(type) }
    if (this.hasToSetSecond(type)) { this.secondPokemonType.as(type) }

    this.drawChosen(type)

    this.drawnWeaknesses()
  }

  resetChosenTypes() {
    this.initializeChosenPokemonTypes()

    this.removeAllChosenTypes()
  }

  drawChosen(type) {
    this.elements.addStyleTo(type, PageTemplate.CHOOSE_CLASS)
  }

  initializeChosenPokemonTypes() {
    this.firstPokemonType = ChosenType.asNull()
    this.secondPokemonType = ChosenType.asNull()
  }

  removeAllChosenTypes() {
    const pokemonTypes = this.elements.getAllButtons()

    pokemonTypes.forEach(pokemonType => {
      this.elements.removeStyleTo(pokemonType.id, PageTemplate.CHOOSE_CLASS)
    })
  }

  removeDrawnWeaknesses() {
    const nothing = ''

    this.elements.drawIn(PageTemplate.WEAKNESSES, nothing)
  }

  drawnWeaknesses() {
    this.removeDrawnWeaknesses()

    this.weaknesses.forEach(type => {
      const template = WeaknessTemplate.retrieve(type)

      this.elements.addIn(PageTemplate.WEAKNESSES, template)
    })
  }

  get weaknesses() {
    const firstType = this.firstPokemonType.toString()
    const secondType = this.secondPokemonType.toString()

    return PokemonsWeaknesses.retrieve(firstType, secondType)
  }

  isFirstTypeChosen() {
    return !this.firstPokemonType.isNull()
  }

  hasToSetFirstType() {
    return !this.isFirstTypeChosen()
  }

  isSecondTypeChosen() {
    return !this.secondPokemonType.isNull()
  }

  hasToSetSecond(type) {
    return !this.isSecondTypeChosen() && !this.isFirstType(type)
  }

  areTypesChosen() {
    return this.isFirstTypeChosen() && this.isSecondTypeChosen()
  }

  isFirstType(type) {
    return this.firstPokemonType.is(type)
  }
}
