
const INITIAL_VALUE = 0

export class Counter {
  static atZero() {
    return new Counter()
  }

  constructor() {
    this.value = INITIAL_VALUE
  }

  increase() {
    this.value ++
  }

  reset() {
    this.value = INITIAL_VALUE
  }

  isZero() {
    return this.value === 0
  }

  isFour() {
    return this.value === 4
  }
}
