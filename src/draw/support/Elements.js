
export class Elements {
  static prepare() {
    return new Elements(document)
  }

  constructor(document) {
    this.document = document
  }

  valueFrom = (id) => {
    return this.get(id).value
  }

  drawRowsIn = (id, values) => {
    const table = this.get(id)
    table.innerHTML = ""

    values.forEach((value) => {
      const row = table.insertRow(0)
      const cell = row.insertCell(0)
      cell.innerHTML = value
    })
  }

  drawIn = (id, value) => {
    this.get(id).innerHTML = value
  }

  addIn = (id, value) => {
    this.get(id).innerHTML += value
  }

  addStyleTo(id, style) {
    this.get(id).classList.add(style)
  }

  removeStyleTo(id, style) {
    this.get(id).classList.remove(style)
  }

  addOnClickTo = (id, callback) => {
    const element = this.get(id)
    element.addEventListener("click", callback)
  }

  getAll(selector) {
    return this.document.querySelectorAll(selector)
  }

  getAllButtons() {
    return this.getAll('button')
  }

  get = (id) => {
    return this.document.getElementById(id)
  }
}
