# Pokemon Weaknesses

## System requirements

- `python2` (optional)

> If you haven't `python2` installed read how to run the project.

## How to run the project

For run the project just run the following command:

- `sh scripts/up`

You can check the project in `http://localhost:8080/src`

> If you haven't the system requirements you only needs to serve this project in any web server

## How to ron the tests

After run the project just open in your browser `http://localhost:8080/SpecRunner.html`
