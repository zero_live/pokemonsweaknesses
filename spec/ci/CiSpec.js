
describe('Ci tests', () => {
  it('has no failures', () => {

    cy.visit('./SpecRunner.html')

    cy.contains('0 failures')
  })

  it("shows the Pokemon's weaknesses", () => {
    const typesCount = 18
    const weaknessesCount = 4

    cy.visit('./src/index.html')
    cy.get('#normal').click()
    cy.get('#fire').click()

    cy.get('#weakness-fighting')
    cy.get('#weakness-stone')
    cy.get('#weakness-water')
    cy.get('#weakness-earth')
    cy.get('button').should('have.length', typesCount)
    cy.get('a').should('have.length', weaknessesCount)
  })
})
