import { Types } from '../../src/core/Types.js'

describe('Types', () => {
  it('knows the Pokemons types', () => {
    expect(Types.steel).toBe('steel')
    expect(Types.water).toBe('water')
    expect(Types.bug).toBe('bug')
    expect(Types.dragon).toBe('dragon')
    expect(Types.electric).toBe('electric')
    expect(Types.ghost).toBe('ghost')
    expect(Types.fire).toBe('fire')
    expect(Types.fairy).toBe('fairy')
    expect(Types.ice).toBe('ice')
    expect(Types.fighting).toBe('fighting')
    expect(Types.normal).toBe('normal')
    expect(Types.plant).toBe('plant')
    expect(Types.psychic).toBe('psychic')
    expect(Types.stone).toBe('stone')
    expect(Types.sinister).toBe('sinister')
    expect(Types.earth).toBe('earth')
    expect(Types.poison).toBe('poison')
    expect(Types.flying).toBe('flying')
  })
})
