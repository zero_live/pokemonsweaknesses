import { PokemonsWeaknesses } from '../../src/core/PokemonsWeaknesses.js'
import { Types } from '../../src/core/Types.js'

describe('PokemonsWeaknesses', () => {
  it('retrieves the weaknesses for a pokemon element', () => {

    const weaknesses = PokemonsWeaknesses.retrieve(Types.normal)

    expect(weaknesses).toEqual([Types.fighting])
  })

  it('retrieves the weaknesses for pokemon elements', () => {

    const weaknesses = PokemonsWeaknesses.retrieve(Types.normal, Types.water)

    expect(weaknesses).toEqual([Types.fighting, Types.electric, Types.plant])
  })

  it('retrieves two times the same weakness', () => {

    const weaknesses = PokemonsWeaknesses.retrieve(Types.normal, Types.normal)

    expect(weaknesses).toEqual([Types.fighting])
  })

  it('ensures all weaknesses', () => {
    expect(PokemonsWeaknesses.retrieve(Types.steel)).toEqual([Types.fire, Types.fighting, Types.earth])
    expect(PokemonsWeaknesses.retrieve(Types.water)).toEqual([Types.electric, Types.plant])
    expect(PokemonsWeaknesses.retrieve(Types.bug)).toEqual([Types.electric, Types.ice, Types.stone])
    expect(PokemonsWeaknesses.retrieve(Types.dragon)).toEqual([Types.ice, Types.dragon, Types.fairy])
    expect(PokemonsWeaknesses.retrieve(Types.electric)).toEqual([Types.earth])
    expect(PokemonsWeaknesses.retrieve(Types.ghost)).toEqual([Types.ghost, Types.sinister])
    expect(PokemonsWeaknesses.retrieve(Types.fire)).toEqual([Types.stone, Types.water, Types.earth])
    expect(PokemonsWeaknesses.retrieve(Types.fairy)).toEqual([Types.poison, Types.steel])
    expect(PokemonsWeaknesses.retrieve(Types.ice)).toEqual([Types.fighting, Types.fire, Types.stone])
    expect(PokemonsWeaknesses.retrieve(Types.fighting)).toEqual([Types.flying, Types.psychic, Types.fairy])
    expect(PokemonsWeaknesses.retrieve(Types.normal)).toEqual([Types.fighting])
    expect(PokemonsWeaknesses.retrieve(Types.plant)).toEqual([Types.fire, Types.poison, Types.flying, Types.ice, Types.bug])
    expect(PokemonsWeaknesses.retrieve(Types.psychic)).toEqual([Types.bug, Types.sinister, Types.ghost])
    expect(PokemonsWeaknesses.retrieve(Types.stone)).toEqual([Types.water, Types.plant, Types.fighting, Types.earth, Types.steel])
    expect(PokemonsWeaknesses.retrieve(Types.sinister)).toEqual([Types.fighting, Types.bug, Types.fairy])
    expect(PokemonsWeaknesses.retrieve(Types.earth)).toEqual([Types.water, Types.plant, Types.ice])
    expect(PokemonsWeaknesses.retrieve(Types.poison)).toEqual([Types.earth, Types.psychic])
    expect(PokemonsWeaknesses.retrieve(Types.flying)).toEqual([Types.electric, Types.ice, Types.stone])
  })
})
