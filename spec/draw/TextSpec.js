import { t } from '../../src/draw/Texts.js'

describe('Texts', () => {
  it('retrieves a translation for a label', () => {

    const translation = t('chooseTypes')

    expect(translation).toBe('Choose pokemon\'s types')
  })
})
